DOT-FILES
=========

.vimrc
------
###Must haven bundles

###Non-sorted: Frequently used hotkeys and commands
- **`<C-]>`, `<C-t>`, `gf`, `\of` (FSwitch), `gf`, `<C-c>g` (python-mode), `<C-c>d`, `<C-\>...` (CScope), `<F4>` (TagbarOpen):** Tags/Doc navigation
- **`gg=G`** -- fix code indent

###Code navigation
| Mode               | Command     | Description                        |
| :--                | :---------- | :--------------------------------- |
| n,v,operator modes | `[[`        | Jump on previous class or function |
| n,v,operator modes | `]]`        | Jump on next class or function     |
| n,v,operator modes | `[M`        | Jump on previous class or method   |
| n,v,operator modes | `]M`        | Jump on next class or method       |
| n,v,operator modes | `{`         |                                    |
| n,v,operator modes | `}`         |                                    |
| n,v,operator modes | `%`/`$`     |                                    |
| n,v,operator modes | `<C-u>`     |                                    |
| n,v,operator modes | `<C-d>`     |                                    |


###Copy/paste

Original: http://vim.wikia.com/wiki/Replace_a_word_with_yanked_text

| mode | Command  | Desctiption                                |
|:-----|:---------|:-------------------------------------------|
| n  | `"+yiw`  | Yank inner word to external clipboard ("+) |
| n  | `"+p`    | Paste from external clipboard              |
| i  | `<C-r>+` | Paste from external clipboard              |

####Copy a word and paste it over other words:

1. `yiw`  yank inner word (copy word under cursor, say "first").
2. ...  Move the cursor to another word (say "second").
3. `viwp`     select "second", then replace it with "first".
4. ...  Move the cursor to another word (say "third").
5. `viw"0p`   select "third", then replace it with "first".

####Copy a line and paste it over other lines:

1. `yy`   yank current line (say "first line").
2. ...  Move the cursor to another line (say "second line").
3. `Vp`   select "second line", then replace it with "first line".
4. ...  Move the cursor to another line (say "third line").
5. `V"0p`     select "third line", then replace it with "first line".

####Alternate method
An alternate method is to do the paste/replace using `cw`. This method has the advantage of being easily repeatable using the `.` repeat command.

1. `yiw`  yank inner word (copy word under cursor, say "first". Same as above).
2. ...  Move the cursor to another word (say "second").
3. `ciw<C-r>0`    select "second", then replace it with "first" If you are at the start of the word then cw<C-r>0 is sufficient.
4. ...  Move the cursor to another word (say "third").
5. `.`    select "third", then replace it with "first".

**NOTE:** `<C-r>0` можно использовать также в коммандном режиме

###Buffers, tabs
| Command                 | Description         |
|:------------------------|:--------------------|
| `<C-^>`                 | # buffer switch     |
| `<C-o>`, `<C-i>`        | jumplist navigation |
| `:BufExplorer` (`\cvv`) |                     |
| `\t`, `gt`, `<C-W>T`    | tabs navigation     |

###Find/replace
- `f`, `F`
- `*`, `#` - find next/prev instance of word under cursor
- `/`, `?`
- `n`, `N`

###Key-bindings
- `:map` - key mappings
- `:nmap` - for normal mode mappings
- `:vmap` - for visual mode mappings
- `:imap` - for insert mode mappings

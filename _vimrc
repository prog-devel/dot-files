".vim: ts=4:sw=4:smartindent:smarttab:expandtab

" path variable  (gf)
set path+=/usr/include/GNUstep/**,/usr/include/x86_64-linux-gnu/**

" PLUGIN Manager VUNDLE: {{{
set nocompatible               " be iMproved
filetype off                   " required!

set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()

" let Vundle manage Vundle
" required!
Plugin 'gmarik/vundle'
Plugin 'L9'

" BUNDLE Python:
" =============
" Powerfull python-mode
" Тянет также Rope -- рефакторинг для питона
Plugin 'klen/python-mode'

" BUNDLE Config:
" =============
" Пока не разобрался, как этим пользоваться
" сделал вручную (.vimrc.proj)
"Plugin 'corntrace/localvimrc.vim'
" put '_local_vimrc.vim'
"Plugin 'local_vimrc.vim'

" BUNDLE Project Navigation:
" =========================
Plugin 'jlanzarotta/bufexplorer'

" Filemanager
Plugin 'scrooloose/nerdtree'

" Source/Header Switcher
Plugin 'FSwitch'

"Plugin 'corntrace/FuzzyFinder'

" более свежий аналог FuzzyFinder
Plugin 'kien/ctrlp.vim'

" Egrep alternative
Plugin 'ack.vim'

" EasyMotion provides a much simpler way to use some motions in vim
" Очень простой в использовании, но очень мощный плагин
Plugin 'Lokaltog/vim-easymotion'

" multiple-cursors
Plugin 'terryma/vim-multiple-cursors'
" goyo-mode for fullscreen win switch
Plugin 'junegunn/goyo.vim'

" Ctags Like:
" ----------
" Extension over ctag for code struct navigation
Plugin 'Tagbar'

" крутое расширение (альтернатива ctags на стероидах) (eclipse-like)
" XXX Работает и без этого (встроено в vim)
"Plugin 'cscope.vim'

" семантический автокомплит с использованием clang
"Plugin 'Rip-Rip/clang_complete'

" Быстрый и умный compliter для разных языков
" говорят, лучший
"Plugin 'Valloric/YouCompleteMe'

" BUNDLE GIT:
" ==========
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'

" BUNDLE Style:
" ============
"Plugin 'corntrace/desert256.vim'
"Plugin 'corntrace/Xoria256m'
Plugin 'flazz/vim-colorschemes'

" Красивая статусная строка
"Plugin 'Lokaltog/vim-powerline'
Plugin 'bling/vim-airline'

Plugin 'edkolev/tmuxline.vim'

" BUNDLE Formatting:
" =============================
Plugin 'Match-Bracket-for-Objective-C'
Plugin 'scrooloose/nerdcommenter'

" Text/Code/Tables alignment
"Plugin 'junegunn/vim-easy-align'
Plugin 'godlygeek/tabular'

" automatic closing of quotes, parenthesis, brackets, etc.
"Plugin 'Raimondi/delimitMate'
Plugin 'jiangmiao/auto-pairs'
"Plugin 'Townk/vim-autoclose'
Plugin 'tpope/vim-surround'
" лечит повтор ненативных команд, например, surrounding
Plugin 'tpope/vim-repeat'

" Snippets:
" --------
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
"Plugin 'UltiSnips'

" Optional:
" --------
" набор сниппетов
Plugin 'honza/vim-snippets'
"Plugin 'right_align'

" BUNDLE LISP:
" ============
Plugin 'kien/rainbow_parentheses.vim'
Plugin 'jpalardy/vim-slime'

" required!
call vundle#end()
filetype plugin on
filetype plugin indent on
" }}}

" Common: {{{
" ======
syntax on
filetype on
set autoread

set matchpairs+=<:>
"set matchpairs+=[:] " by default
set matchpairs+=,:,

set number

" set autoindent
set smartindent

" Enables Vim to perform C program indenting automatically.
set cindent
" cindent options
"   :0 - Place case labels 0 characters from the indent of the switch().
"   l1 - If N != 0 Vim will align with a case label instead of the
"        statement after it in the same line.
"   (0 - When in unclosed parentheses, indent N characters from the line
"        with the unclosed parentheses.
set cinoptions=l1,:0,(0

set tabstop=4
set shiftwidth=4

" при нажатии Backspace удаляет все пробелы, идущие от начала строки, размером shiftwith
" воспринимая их как табуляцию
set smarttab

" Символ табуляции подменяется пробелами (длиной shiftwidth)
" при команде << или >>
set expandtab

set cul " cuc             " Cursor column & line highlighting
set ignorecase          " Searches are Non Case-sensitive
set hlsearch            " Highlight search terms (very useful!)

" способ фолдинга: по маркерам (по умолчанию { {{ }} })
set foldmethod=marker

" не пытаться закрыть буффер, если окно переключаем на другой
" т.о. можно несохраненные данные иметь в спрятанных буфферах
set hidden

" Принудительно оповещаем о том, что терминал у нас как бы 256 цветов
" По умолчанию screen -- 8 цветов
if &term == 'screen' || &term == 'xterm'
      set t_Co=256
endif

" Syntax Highlight:
" ----------------
" set syntax hightlight for certain file types
au BufNewFile,BufRead *.nu,*.nujson set filetype=lisp
au BufNewFile,BufRead SCons* set filetype=python

au FileType objc,c setlocal ts=8 sts=8 sw=8
au FileType python setlocal ts=4 sts=4 sw=4
" }}}

" PLUGIN CTags: {{{
" =====================
" ctags из коробки умеет <C-]>, <C-T> и т.п.
" XXX сейчас используется cscope
set tags=./tags;tags;
let Tlist_Ctags_Cmd = "/usr/bin/ctags"
let Tlist_WinWidth = 50
let tlist_objc_settings='ObjectiveC;P:protocols;i:interfaces;I:implementations;m:obj-methods;c:class-methods;F:fields;v:Glob-vars'

" Key Bind:
" --------
" Taglist
"map <F4> :TlistToggle<cr>
" CTags update (for Taglist, для переходов по <C-]>/<C-T>, .h <-> .c)
" Use cscope
" nnoremap <F5> :!ctags --extra=+f -R --langmap=ObjectiveC:.m.h --exclude="@.srclist_ex" -L .srclist --totals --languages=-javascript<cr>
" }}}

" PLUGIN CScope: {{{
" ======
" Cscope is a developer's tool for browsing source code
" Является более умной альтернативой вместо ctags
" Для пояснений сюда: http://cscope.sourceforge.net/cscope_maps.vim

if has('cscope')
    " add any cscope database in current directory
    if filereadable('./cscope.out')
        cs add cscope.out
        " else add the database pointed to by environment variable
    elseif $CSCOPE_DB != ""
        cs add $CSCOPE_DB
    endif

    " Обновление базы cscope.out
    " Пока так. Говорят, можно сделать автоматом
    map <F5> :silent !find . -not -path 'build' -iname '*.c' -o -iname '*.m' -o -iname '*.h' > cscope.files<CR>
                \:!cscope -b -q -i cscope.files -f cscope.out<CR>
                \:cs reset<CR>

    " cscopetag -- use cscope DB for tag search
    " for support 'ctrl-]', ':ta', 'vim -t'
    set cscopetag 
    " cscopeverbose -- show msg when any other cscope db added
    set cscopeverbose

    if has('quickfix')
        set cscopequickfix=s-,c-,d-,i-,t-,e-
    endif

    " Key Bindings:
    " ------------
    " The following maps all invoke one of the following cscope search types:
    "
    "   's'   symbol: find all references to the token under cursor
    "   'g'   global: find global definition(s) of the token under cursor
    "   'c'   calls:  find all calls to the function name under cursor
    "   't'   text:   find all instances of the text under cursor
    "   'e'   egrep:  egrep search for the word under cursor
    "   'f'   file:   open the filename under cursor
    "   'i'   includes: find files that include the filename under cursor
    "   'd'   called: find functions that function under cursor calls
    "

    " search results in same window
    nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>

    " separate vertically
    nmap <C-@>s :scs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>g :scs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>c :scs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>t :scs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>e :scs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@>f :scs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-@>i :scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-@>d :scs find d <C-R>=expand("<cword>")<CR><CR>

    " separate horizontally
    nmap <C-@><C-@>s :vert scs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>g :vert scs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>c :vert scs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>t :vert scs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>e :vert scs find e <C-R>=expand("<cword>")<CR><CR>
    nmap <C-@><C-@>f :vert scs find f <C-R>=expand("<cfile>")<CR><CR>
    nmap <C-@><C-@>i :vert scs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <C-@><C-@>d :vert scs find d <C-R>=expand("<cword>")<CR><CR>

    cnoreabbrev csa cs add
    cnoreabbrev csf cs find
    cnoreabbrev csk cs kill
    cnoreabbrev csr cs reset
    cnoreabbrev css cs show
    cnoreabbrev csh cs help

    "  command -nargs=0 Cscope cs add $VIMSRC/src/cscope.out $VIMSRC/src
endif
" }}}

" FuzzyFinder: {{{
"nmap <Leader>ff :FufFile<cr>
" }}}

" PLUGIN Ack: {{{
" ===
let g:ackprg='ack -H --nocolor --nogroup --column --type-add python=.pyx,.pxd,.pxi --type-add lisp=.podsl,.nu'
" }}}

" Clang-completion
"let g:clang_complete_loaded=1
let g:clang_library_path='/usr/lib/'
let g:clang_complete_copen=1
let g:clang_complete_auto=1
let g:clang_hl_errors=1
" Avoid freezing on offending code
let g:clang_user_options='|| exit 0'
let g:clang_close_preview=1

" ??? OmniCompletion
" set omnifunc=syntaxcomplete#Complete

" PLUGIN FSwitch: smart source/header switch {{{
augroup fsw_generic
au!
    au BufEnter *.h let b:fswitchdst  = 'm,c,cpp'
    au BufEnter *.h let b:fswitchlocs = './,reg:|Headers|Sources|'
    au BufEnter *.m,*.c,*.cpp let b:fswitchdst  = 'h'
    au BufEnter *.m,*.c,*.cpp let b:fswitchlocs = './,reg:|Sources|Headers|'
augroup END
" Переключение между сырцами/хедэрами
nmap <silent> <Leader>of :FSHere<cr>
" }}}

" Style: {{{
" =====
function! g:style_setup()
    " Color-scheme
<<<<<<< Updated upstream
    set background=light
=======
    set background=dark
>>>>>>> Stashed changes
    "colorscheme xoria256m
    "colors xoria256
    "colors desert
    let g:solarized_termcolors=256
    colors solarized

    " Highlight spacec at end of lines
    "let c_space_errors = 1
    highlight RedundantSpaces term=standout ctermbg=red guibg=red
    match RedundantSpaces /\s\+$\| \+\ze\t/
    " ограничение ширины исходного кода
    "highlight OverLength ctermbg=red ctermfg=white guibg=#592929
    "match OverLength /\%81v.\+/

    set colorcolumn=80
endfunction
call g:style_setup()
" }}}

" PLUGIN Airline: {{{
" =======
let g:airline_powerline_fonts = 1
"set guifont=Sauce\ Code\ Powerline:h12
let g:airline#extensions#tabline#enabled = 1
<<<<<<< Updated upstream
"let g:airline_theme = 'ubaryd'
"let g:airline_theme = 'jellybeans'
let g:airline_theme = 'solarized'
=======
let g:airline_theme = 'ubaryd'
"let g:airline_theme = 'jellybeans'
"let g:airline_theme = 'solarized'
>>>>>>> Stashed changes
" }}}

" PLUGIN GitGutter: {{{
" =========
function! g:gitgutter_style_setup()
    " Bg color of gutter column
    " XXX Должно быть после установки темы
    " XXX Иначе не сработает
    highlight clear SignColumn
endfunction

call g:gitgutter_style_setup()

nmap [h <Plug>GitGutterPrevHunk
nmap ]h <Plug>GitGutterNextHunk
" }}}

" PLUGIN Python Mode: {{{
" ==================
" Activate rope
" Keys:
" K             Show python docs
" <Ctrl-Space>  Rope autocomplete
" <Ctrl-c>g     Rope goto definition
" <Ctrl-c>d     Rope show documentation
" <Ctrl-c>f     Rope find occurrences
" <Leader>b     Set, unset breakpoint (g:pymode_breakpoint enabled)
" [[            Jump on previous class or function (normal, visual, operator modes)
" ]]            Jump on next class or function (normal, visual, operator modes)
" [M            Jump on previous class or method (normal, visual, operator modes)
" ]M            Jump on next class or method (normal, visual, operator modes)
let g:pymode_rope = 1
let g:pymode_rope_complete_on_dot = 0
let g:pymode_rope_goto_definition_cmd = 'vnew'
"
" " Documentation
let g:pymode_doc = 1
let g:pymode_doc_key = 'K'
"
" "Linting
let g:pymode_lint = 1
let g:pymode_lint_checker = "pyflakes,pep8"
" " Auto check on save
let g:pymode_lint_write = 1
" Отключаем надоедливое окно, отображающее ошибки и предупреждения
let g:pymode_lint_cwindow = 0
"
" " Support virtualenv
let g:pymode_virtualenv = 1
"
" " Enable breakpoints plugin
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_key = '<leader>b'
"
" " syntax highlighting
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all
"
" " Don't autofold code
let g:pymode_folding = 0
" }}}

" PLUGIN Tagbar: {{{
" =============
"autocmd VimEnter * nested :call tagbar#autoopen(1)
"autocmd FileType * nested :call tagbar#autoopen(0)
"autocmd BufEnter * nested :call tagbar#autoopen(0)
" tagbar shows tags in order of they created in file
let g:tagbar_sort = 0
let g:tagbar_autoclose = 1
let g:tagbar_autofocus = 1

" BEGIN copied from http://www.daskrachen.com/2011/12/how-to-make-tagbar-work-with-objective.html
" add a definition for Objective-C to tagbar
let g:tagbar_type_objc = {
    \ 'ctagstype' : 'ObjectiveC',
    \ 'kinds'     : [
        \ 'i:interface',
        \ 'I:implementation',
        \ 'p:Protocol',
        \ 'm:Object_method',
        \ 'c:Class_method',
        \ 'v:Global_variable',
        \ 'F:Object field',
        \ 'f:function',
        \ 'p:property',
        \ 't:type_alias',
        \ 's:type_structure',
        \ 'e:enumeration',
        \ 'M:preprocessor_macro',
    \ ],
    \ 'sro'        : ' ',
    \ 'kind2scope' : {
        \ 'i' : 'interface',
        \ 'I' : 'implementation',
        \ 'p' : 'Protocol',
        \ 's' : 'type_structure',
        \ 'e' : 'enumeration'
    \ },
    \ 'scope2kind' : {
        \ 'interface'      : 'i',
        \ 'implementation' : 'I',
        \ 'Protocol'       : 'p',
        \ 'type_structure' : 's',
        \ 'enumeration'    : 'e'
    \ }
\ }
" END copied from http://www.daskrachen.com/2011/12/how-to-make-tagbar-work-with-objective.html

map <F4> :TagbarOpen j<cr>
" }}}

" PLUGIN NERDTree: {{{
" игнорировать при навигации по файловой системе
let NERDTreeIgnore=['\.pyc$']
let g:NERDTreeMinimalUI=1

map <F3> :NERDTree<cr>
" close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
" open a NERDTree automatically when vim starts
autocmd vimenter * if !argc() | NERDTree | endif
" }}}

" PLUGIN EasyAlign: {{{
" ================
" Start interactive EasyAlign in visual mode
"vmap <Enter> <Plug>(EasyAlign)
" Start interactive EasyAlign with a Vim movement
"nmap <Leader>a <Plug>(EasyAlign)
" }}}

" PLUGIN Goyo: {{{
function! g:goyo_before()
    if exists('$TMUX')
        "silent !tmux set status off
    endif
    "set noshowmode
    "set noshowcmd
    " ...
endfunction

function! g:goyo_after()
    if exists('$TMUX')
        "silent !tmux set status on
    endif
    " FIXME Почему ломается стиль?
    call g:style_setup()
    call g:gitgutter_style_setup()
    "set showmode
    "set showcmd
    " ...
endfunction

let g:goyo_callbacks = [function('g:goyo_before'), function('g:goyo_after')]
let g:goyo_width = 100

nnoremap <silent> <Leader><Space> :Goyo<CR>

" }}}

" PLUGIN Rainbow Parentheses: {{{
autocmd Syntax lisp RainbowParenthesesLoadRound
"autocmd BufEnter *.lisp,*.lsp,*.l,*.nu RainbowParenthesesToggle
"autocmd BufLeave *.lisp,*.lsp,*.l,*.nu RainbowParenthesesToggle
autocmd BufEnter lisp RainbowParenthesesToggle

let g:rbpt_colorpairs = [
    \ ['magenta',     'purple1'],
    \ ['cyan',        'magenta1'],
    \ ['green',       'slateblue1'],
    \ ['yellow',      'cyan1'],
    \ ['red',         'springgreen1'],
    \ ['magenta',     'green1'],
    \ ['cyan',        'greenyellow'],
    \ ['green',       'yellow1'],
    \ ['yellow',      'orange1'],
    \ ]
let g:rbpt_max = 9
" }}}

" PLUGIN VIM SLIME: {{{
let g:slime_target = "tmux"
" }}}

" Build Debug: {{{
" ===========
set makeprg=make\ -j8
map <Leader>bb :wall \| make<Cr>

" Навигация по ошибкам сборки
map <Leader>p :cn<Cr>zvzz:cc<Cr>
map <Leader>o :cp<Cr>zvzz:cc<Cr>

" gdb
"imap <F6> :!vim_gdb.sh<cr><cr>
"nmap <F6> :!vim_gdb.sh<cr><cr>

" qickfix on :make
" Automatically open, but do not go to (if there are errors) the quickfix /
" location list window, or close it when is has become empty.
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow

" Замена ^L fold-маркером
"function! FoldPageFeed()
"    setl foldmethod=expr
"    setl foldexpr=getline(v:lnum)[0]==\"\\<c-l>\"
"    setl foldminlines=0
"    setl foldtext='---\ new\ page\ '
"    setl foldlevel=0
"    set foldclose=all
"endfunction
" }}}

" KEY BINDINGS: {{{ Additional key-bindings
" paste toggle
set pastetoggle=<F2>
" paste in normal mode from system clipboard
nnoremap <silent> <Leader><C-v> <ESC>:set paste<CR>"+p:set nopaste<CR>

" Buffers/tabs/navigation
map <leader>t :tabnew<CR>
nnoremap <Leader>vv :BufExplorer<CR>
"nnoremap <Leader>vn :bn<CR>
"nnoremap <Leader>vp :bp<CR>
" }}}

if filereadable(expand("~/.vimrc.local"))
    source ~/.vimrc.local
endif

" Load per-project .vimrc
let b:thisdir=expand("%:p:h")
let b:vim=b:thisdir."/.vimrc.proj"
if (filereadable(b:vim))
    execute "source ".b:vim
endif
